# What is Ansible?

It's a Free Software automation platform. Mostly, folks use it for Configuration Management and/or Infrastructure As Code. Compares favorably to Puppet, Salt, and Chef. May also compare favorably to Terraform, Foreman, RunDeck, CloudFormation, and Heat, depending on use and preference.

# Why Ansible? / What rocks?

- Agentless (sshd, python, some libs)
- Gentle learning curve
  * ad-hoc
  * playbooks
  * vaults
  * roles
  * collections
  * tower
- Supported by Red Hat
- Popular with the Community

# Why not Ansible? / What sucks?

- ssslllooooowwww...
- ch-ch-chatty... (probably part of why it's so slow)
- stack dumps like a Python (or a Java), maybe not quite that bad

# Exercises

Go here: https://etherpad.opendev.org/p/VWeTpwBj_Yhor3cxHU5K

Choose a username and enter a MacGuffin next to it. It could be the MacGuffin of a popular story or something you made up, but it must be unique. Mine is 'alicorn', from the 1985 movie Legend, starring Tim Curry, Mia Sara, and Tom Cruise.

Connect to the Ansible Controller, change directory to and update the materials

```bash
ssh USERNAME@ansictl.sofree.us
cd ~/blug-2020-11-12/
git pull
```

Decrypt credentials for Fuga Cloud (DLW's favorite OpenStack provider)

```bash
ansible-vault decrypt clouds.yaml.vault --output clouds.yaml
```

Set your MacGuffin (your namespace). 

```bash
# un-comment the "# macguffin:" line and set a unique value for your MacGuffin
# example: "macguffin: alicorn"
# edit host_vars/localhost.yml # vim or emacs or joe or nano or ...
# confirm the value with the debug module
ansible localhost -m debug -a 'var=macguffin'
```

Create and configure a webserver.

```bash
# Create an ssh key and upload the public key as a key pair for use with OpenStack
ansible-playbook create-ssh-key.yml

# Create a server machine, authorize the ssh key with the default admin account
ansible-playbook create-server.yml

# Confirm that your server machine has been created.
ansible all --list

# Start a keyring and add the ssh key to it
eval $( ssh-agent )
ssh-add *.key

# IMPORTANT: Set an export environment variable for your MacGuffin
# like this: export MACGUFFIN=$YOUR_ACTUAL_MACGUFFIN
# example:   export MACGUFFIN=alicorn

# Use the ping module to confirm that Ansible can connect to your server machine
ansible $MACGUFFIN -m ping -u fedora
# or just type your actual MacGuffin in place of $MACGUFFIN

# Use the setup module to view available Ansible facts
ansible $MACGUFFIN -m setup -u fedora
# NOTE: The setup module runs by default at the beginning of every playbook,
# so you have access to all those facts as variables, by default.

# Configure the machine as a webserver
# edit configure-webserver.yml
# update the host: and image_url: appropriately, then run the playbook
ansible-playbook configure-webserver.yml -u fedora

# Use the debug module from within a playbook to get the machine's address
ansible-playbook show-ip-addresses.yml -u fedora
```

Test the webserver by pointing a web browser at that IP address.

Go here: https://etherpad.opendev.org/p/VWeTpwBj_Yhor3cxHU5K

Add the IP address of your webserver. Pound your chest and do a gorilla victory scream [like this](gorilla-victory-rehearsal.jpg), or call your mom.

# Cleanup

```
# Delete your machine
ansible-playbook delete-server.yml
# Delete your ssh-key
ansible-playbook delete-ssh-key.yml
```

# Further Reading

[Fuga Cloud Ansible Tuts](https://docs.fuga.cloud/tutorials#ansible)

[Ansible for DevOps](https://www.jeffgeerling.com/blog/2020/self-publishing-and-2nd-edition-ansible-devops)
